#include "ShaderManager.h"
#include "../../JS/Shader/OpenGL/ShaderUtil.h"
#include "../../JS/Shader/OpenGL/BasicColorProgram.h"
#include "../../JS/Shader/OpenGL/BasicTextureProgram.h"

namespace terrain_tiles_test
{
    namespace Shader
    {
        Manager * Manager::m_Instance = nullptr;
        
        Manager::Manager()
        {
            uint32_t bcvsID, bcfsID;
            JS::Shader::CompileVertexShader(bcvsID, JS::Shader::BasicColorProgram::shader_code_vs);
            JS::Shader::CompileFragmentShader(bcfsID, JS::Shader::BasicColorProgram::shader_code_fs);
            
            JS::Shader::BasicColorProgram* bcp = new JS::Shader::BasicColorProgram();
            bcp->init(bcvsID, bcfsID);
            
            m_mapProgram[ecPROGRAM::BASIC_COLOR] = bcp;
            
            //////////////////////////////////////////////////////////////////////////////////////////////////
            
            uint32_t btvsID, btfsID;
            JS::Shader::CompileVertexShader(btvsID, JS::Shader::BasicTextureProgram::shader_code_vs);
            JS::Shader::CompileFragmentShader(btfsID, JS::Shader::BasicTextureProgram::shader_code_fs);
            
            JS::Shader::BasicTextureProgram* btp = new JS::Shader::BasicTextureProgram();
            btp->init(btvsID, btfsID);
            
            m_mapProgram[ecPROGRAM::BASIC_TEXTURE] = btp;
        }
        
        Manager::~Manager()
        {
            for (auto program : m_mapProgram)
            {
                if (nullptr != program.second)
                {
                    delete program.second;
                }
            }
            
            if (nullptr == m_Instance)
            {
                delete m_Instance;
            }
        }
        
        Manager * Manager::Instance()
        {
            if (nullptr == m_Instance)
            {
                m_Instance = new Manager;
            }
            
            return m_Instance;
        }
        
        JS::Shader::Program* Manager::GetProgram(const ecPROGRAM & program)
        {
            auto it = m_mapProgram.find(program);
            if (it != m_mapProgram.end())
            {
                return it->second;
            }
            else
            {
                return nullptr;
            }
        }
    }
}
