//
//  main.cpp
//  terrain-tiles-test
//
//  Created by hjs on 4/1/18.
//  Copyright © 2018 com.hjs. All rights reserved.
//

#include <iostream>
#include <fstream>


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <tileindexcalc.hpp>
#include <tileid.hpp>

#include "../../src/ShaderManager.h"
#include "../../src/HttpRequest.h"
#include "../../../JS/Shader/OpenGL/BasicColorProgram.h"
#include "../../../JS/Shader/OpenGL/BasicTextureProgram.h"
#include <glad/glad.h>
#include <glfw/glfw3.h>

#include <curl/curl.h>

#include <vector>

#include <glm/vec3.hpp> // glm::vec3
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective


static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}


int main(int argc, const char * argv[])
{
    const char * NEXTZEN_API_KEY = getenv("NEXTZEN_API_KEY");
    
    if(nullptr == NEXTZEN_API_KEY)
    {
        std::cout << "Fail : NEXTZEN_API_KEY is null.";
        return 0;
    }
    int zoom = 12;
    double lon = 138.7100106;
    double lat = 35.3606937;
    int index_x = TileIndexCalc::Google::long2tilex(lon, zoom);
    int index_y = TileIndexCalc::Google::lat2tiley(lat, zoom);
    
    TileID tid(zoom, index_x, index_y);
    const std::string & tilename = tid.getName();
    const std::string & test_url =
    "https://tile.nextzen.org/tilezen/terrain/v1/256/terrarium" + tid.getName() + ".png?api_key=" + NEXTZEN_API_KEY;
    
    curl_global_init(CURL_GLOBAL_ALL);
    
    terrain_tiles_test::HttpRequest hr;
        
    hr(test_url.c_str());
    
    /* we're done with libcurl, so clean it up */
    curl_global_cleanup();
    
    int width, height, comp;
    unsigned char* pixels = nullptr;
    
    if(hr.result.size > 0)
    {
        printf("%lu bytes retrieved\n", (long)hr.result.size);
        pixels = stbi_load_from_memory((unsigned char*)hr.result.memory, hr.result.size, &width, &height, &comp, 0);
    }
    else
    {
        return 0;
    }
    
    float vertex_quad[] =
    {
        1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
        1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
        1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
    };
    
    GLFWwindow* window;
    GLuint vertex_buffer_quad, vertex_buffer_axis;
    
    glfwSetErrorCallback(error_callback);
    
    if (!glfwInit())
        exit(EXIT_FAILURE);
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    
    window = glfwCreateWindow(640, 480, "Simple example", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    
    glfwSetKeyCallback(window, key_callback);
    
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);
    
    // NOTE: OpenGL error checks have been omitted for brevity
    unsigned int texid;
    glGenTextures(1, &texid);
    glBindTexture(GL_TEXTURE_2D, texid);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    delete[] pixels;
    
    glGenBuffers(1, &vertex_buffer_quad);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_quad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_quad), vertex_quad, GL_STATIC_DRAW);
    
    const float vertex_axis[] =
    {
        0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
        9.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 9.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 9.0f, 0.0f, 0.0f, 1.0f
    };
    glGenBuffers(1, &vertex_buffer_axis);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_axis);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_axis), vertex_axis, GL_STATIC_DRAW);
    
    const glm::mat4 & matView = glm::lookAt(glm::vec3(5.0f, 5.0f, 5.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
    
    JS::Shader::BasicColorProgram * bcp = (JS::Shader::BasicColorProgram *)terrain_tiles_test::Shader::Manager::Instance()->GetProgram(terrain_tiles_test::Shader::ecPROGRAM::BASIC_COLOR);
    
    JS::Shader::BasicTextureProgram * btp = (JS::Shader::BasicTextureProgram *)terrain_tiles_test::Shader::Manager::Instance()->GetProgram(terrain_tiles_test::Shader::ecPROGRAM::BASIC_TEXTURE);
    
    while (!glfwWindowShouldClose(window))
    {
        float ratio;
        int width, height;
        
        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;
        
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texid);
        
        const glm::mat4 & matProjection = glm::perspective(glm::radians(45.0f), ratio, 0.1f, 100.f);
        const glm::mat4 & matVP = matProjection * matView;
        
        bcp->Use();
        
        // axis
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_axis);
        
        glEnableVertexAttribArray(JS::Shader::ATT_POSITION);
        glEnableVertexAttribArray(JS::Shader::ATT_COLOR);
        
        glVertexAttribPointer(JS::Shader::ATT_POSITION, 3, GL_FLOAT, GL_FALSE, 24, (void*) 0);
        glVertexAttribPointer(JS::Shader::ATT_COLOR, 3, GL_FLOAT, GL_FALSE,  24, (void*)12);
        
        bcp->SetUniformMVPMatrix(&matVP[0][0]);
        glDrawArrays(GL_LINES, 0, 6);
        
        btp->Use();
        
        // quad
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_quad);
        const glm::mat4 & matModelQuad = glm::rotate(glm::mat4(), (float) glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));
        const glm::mat4 & matMVPQuad = matProjection * matView * matModelQuad;
        
        glEnableVertexAttribArray(JS::Shader::ATT_POSITION);
        glEnableVertexAttribArray(JS::Shader::ATT_TEXCOORD);
        glDisableVertexAttribArray(JS::Shader::ATT_COLOR);
        
        glVertexAttribPointer(JS::Shader::ATT_POSITION, 3, GL_FLOAT, GL_FALSE, 20, (void*) 0);
        glVertexAttribPointer(JS::Shader::ATT_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 20, (void*) 12);
        glVertexAttrib4f(JS::Shader::ATT_COLOR, 1.0f, 1.0f, 1.0f, 1.0f);
        
        btp->SetActiveTexture(0);
        btp->SetUniformMVPMatrix(&matMVPQuad[0][0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertex_quad) / 20);
        
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    
    glfwDestroyWindow(window);
    
    glfwTerminate();
    exit(EXIT_SUCCESS);
        
    return 0;
}
