#ifndef _TILE_INDEX_CALC_HPP_HJS_20180401_
#define _TILE_INDEX_CALC_HPP_HJS_20180401_

#include <cmath>

namespace TileIndexCalc
{
    namespace Google
    {
        // https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#C.2FC.2B.2B
        int long2tilex(double lon, int z)
        {
            return (int)(floor((lon + 180.0) / 360.0 * pow(2.0, z)));
        }
        
        int lat2tiley(double lat, int z)
        {
            return (int)(floor((1.0 - log( tan(lat * M_PI/180.0) + 1.0 / cos(lat * M_PI/180.0)) / M_PI) / 2.0 * pow(2.0, z)));
        }
        
        double tilex2long(int x, int z)
        {
            return x / pow(2.0, z) * 360.0 - 180;
        }
        
        double tiley2lat(int y, int z)
        {
            double n = M_PI - 2.0 * M_PI * y / pow(2.0, z);
            return 180.0 / M_PI * atan(0.5 * (exp(n) - exp(-n)));
        }
    }
}

#endif
