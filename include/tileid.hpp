#ifndef _TILE_ID_HPP_HJS_20180408_
#define _TILE_ID_HPP_HJS_20180408_

#include <cmath>
#include <cstdint>
#include <string>

class TileID
{
public:
    const int32_t zoom;
    const int32_t x;
    const int32_t y;
    const int32_t reserve;
    
public:
    TileID(const int32_t & _zoom, const int32_t & _x, const int32_t & _y)
    : zoom(_zoom), x(_x), y(_y), reserve(0)
    {
    }
    
    std::string getName() const
    {
        return "/" + std::to_string(zoom) + "/" + std::to_string(x) + "/" + std::to_string(y);
    }
};

#endif
