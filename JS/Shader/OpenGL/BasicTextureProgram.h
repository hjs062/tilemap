#ifndef _JS_BASIC_TEXTURE_PROGRAM_H_HJS_20170609_
#define _JS_BASIC_TEXTURE_PROGRAM_H_HJS_20170609_

#include <cstdint>
#include <string>
#include <map>
#include <array>

#include "ShaderProgram.h"

namespace JS
{
    namespace Shader
    {
        class BasicTextureProgram : public JS::Shader::Program
        {
        private:
            enum eUNIFORM
            {
                UNI_MVPMatrix = 1,
                UNI_TEXTURE = 2
            };
            
        public:
            BasicTextureProgram();
            ~BasicTextureProgram();
            
            void SetUniformMVPMatrix(const float * matrix);
            void SetActiveTexture(const int & activetexture);
            
            const static char * shader_code_vs;
            const static char * shader_code_fs;
            
        protected:
            std::map<uint32_t, int32_t> m_mapBindUniform;
            
        protected:
            void _BindAttributes();
            void _BindUniforms();
        };
    }
}

#endif
