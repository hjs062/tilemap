#ifndef _JS_SHADER_PROGRAM_HJS_20170309_
#define _JS_SHADER_PROGRAM_HJS_20170309_

#include <cstdint>

namespace JS
{
	namespace Shader
	{
		class Program
		{
		public:
			Program();
			virtual ~Program();
            
			bool init(const uint32_t & vs, const uint32_t & fs);
			void Use();

		protected:
			uint32_t m_id;

			virtual void _BindAttributes() = 0;
			virtual void _BindUniforms() = 0;

		};
	}
}

#endif

