#include "BasicColorProgram.h"
#include "ShaderUtil.h"
#include "../../GraphicsAPI/OpenGL/OpenGLHeader.h"
#include "ShaderAttribute.h"

const char * JS::Shader::BasicColorProgram::shader_code_vs = "\
attribute vec4    attPosition;\
attribute vec4    attColor;\
uniform mat4    uniMVPMatrix;\
varying vec4 color;\
void main(void)\
{\
color = attColor;\
gl_Position = uniMVPMatrix * attPosition;\
}";



const char * JS::Shader::BasicColorProgram::shader_code_fs = "\
varying vec4 color;\
void main (void)\
{\
gl_FragColor = color;\
}";

namespace JS
{
    namespace Shader
    {
        BasicColorProgram::BasicColorProgram()
        {
        }
        
        BasicColorProgram::~BasicColorProgram()
        {
        }
        
        void BasicColorProgram::_BindAttributes()
        {
            std::map<uint32_t, std::string> mapAtt;
            
            mapAtt[ATT_POSITION] = "attPosition";
            mapAtt[ATT_COLOR] = "attColor";
            
            JS::Shader::BindAttributeID(mapAtt, m_id);
        }
        
        void BasicColorProgram::_BindUniforms()
        {
            std::map<uint32_t, std::string> mapUniform;
            
            mapUniform[UNI_MVPMatrix] = "uniMVPMatrix";
            
            JS::Shader::GetUniformID(m_mapBindUniform, mapUniform, m_id);
            for (auto & dd : m_mapBindUniform)
            {
                printf("BasicColorProgram::_BindUniforms %u %d\n", dd.first, dd.second);
            }
        }
        
        void BasicColorProgram::SetUniformMVPMatrix(const float * matrix)
        {
            glUniformMatrix4fv(m_mapBindUniform[UNI_MVPMatrix], 1, GL_FALSE, matrix);
        }
    }
}
