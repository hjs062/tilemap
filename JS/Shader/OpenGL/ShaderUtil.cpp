#include "ShaderUtil.h"

#include "../../GraphicsAPI/OpenGL/OpenGLHeader.h"
#include <vector>

namespace JS
{
	namespace Shader
	{
		bool CompileVertexShader(uint32_t & id, const char * shader)
		{
			uint32_t tmp = glCreateShader(GL_VERTEX_SHADER);
			glShaderSource(tmp, 1, (const char**)&shader, NULL);
			glCompileShader(tmp);

			int32_t bShaderCompiled;
			glGetShaderiv(tmp, GL_COMPILE_STATUS, &bShaderCompiled);
			if (!bShaderCompiled)
			{
				int infoLogLength, numCharsWritten;
				glGetShaderiv(tmp, GL_INFO_LOG_LENGTH, &infoLogLength);
				std::vector<char> infoLog; infoLog.resize(infoLogLength);
				glGetShaderInfoLog(tmp, infoLogLength, &numCharsWritten, infoLog.data());

				std::string message("Failed to compile vertex shader: ");
				message += infoLog.data();
				return false;
			}

			id = tmp;
			return true;
		}

		bool CompileFragmentShader(uint32_t & id, const char * shader)
		{
			uint32_t tmp = glCreateShader(GL_FRAGMENT_SHADER);
			glShaderSource(tmp, 1, (const char**)&shader, NULL);
			glCompileShader(tmp);

			int32_t bShaderCompiled;
			glGetShaderiv(tmp, GL_COMPILE_STATUS, &bShaderCompiled);
			if (!bShaderCompiled)
			{
				int infoLogLength, numCharsWritten;
				glGetShaderiv(tmp, GL_INFO_LOG_LENGTH, &infoLogLength);
				std::vector<char> infoLog; infoLog.resize(infoLogLength);
				glGetShaderInfoLog(tmp, infoLogLength, &numCharsWritten, infoLog.data());

				std::string message("Failed to compile vertex shader: ");
				message += infoLog.data();
				return false;
			}

			id = tmp;
			return true;
		}

		void GetUniformID(std::map<uint32_t, int32_t> & out, const std::map<uint32_t, std::string> & in, const uint32_t & pid)
		{
			for (const auto & p : in)
			{
				out[p.first] = glGetUniformLocation(pid, p.second.c_str());
			}
		}

		void BindAttributeID(const std::map<uint32_t, std::string> & in, const uint32_t & pid)
		{
			for (const auto & p : in)
			{
				glBindAttribLocation(pid, p.first, p.second.c_str());
			}
		}
	}
}
